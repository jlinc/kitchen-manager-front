// Ionic Kitchen Manager App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('km', ['ionic', 'ngResource', 'km.controllers', 'km.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})


.constant('API',
  //'http://kitchenmanager-salaesp.rhcloud.com'
  'http://127.0.0.1:8080'
)

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('tab.kitchens', {
      url: '/kitchens',
      views: {
        'tab-kitchens': {
          templateUrl: 'templates/tab-kitchens.html',
          controller: 'KitchensCtrl'
        }
      }
    })
    .state('tab.kitchen', {
        url: '/kitchens/:kitchenId',
        views: {
          'tab-kitchens': {
            templateUrl: 'templates/kitchen.html',
            controller: 'KitchensCtrl'
          }
        }
      })
      .state('tab.menues', {
          url: '/kitchens/:kitchenId/menues',
          views: {
            'tab-kitchens': {
              templateUrl: 'templates/menues.html',
              controller: 'MenuesCtrl'
            }
          }
        })
        .state('tab.menu', {
          url: '/kitchens/:kitchenId/menues/:menuId',
          views: {
            'tab-kitchens': {
              templateUrl: 'templates/menu.html',
              controller: 'MenuesCtrl'
            }
          }
        })

  .state('tab.account', {
      url: '/account',
      views: {
        'tab-account': {
          templateUrl: 'templates/myAccount.html',
          controller: 'AccountCtrl'
        }
      }
    })
    .state('tab.login', {
      url: '/account/login',
      views: {
        'tab-account': {
          templateUrl: 'templates/login.html',
          controller: 'AccountCtrl'
        }
      }
    })
    .state('tab.register', {
      url: '/account/register',
      views: {
        'tab-account': {
          templateUrl: 'templates/register.html',
          controller: 'AccountCtrl'
        }
      }
    });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/dash');

})

.config(['$resourceProvider', function($resourceProvider) {
  // Don't strip trailing slashes from calculated URLs
  $resourceProvider.defaults.stripTrailingSlashes = false;
}]);
