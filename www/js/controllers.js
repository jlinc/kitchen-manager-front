angular.module('km.controllers', [])
  .controller('KitchensCtrl', ['$scope', '$stateParams', 'Kitchens', function($scope, $stateParams, Kitchens) {
    $scope.$on('$ionicView.enter', function(e) {
      if ($stateParams.kitchenId) {
        if(!$scope.kitchen) {
          $scope.kitchen = Kitchens.get({id: $stateParams.kitchenId});
        }
      }
      if(!$scope.kitchens) {
        $scope.kitchens = Kitchens.query();
      }
    });
  }])
  .controller('MenuesCtrl', ['$scope', '$stateParams', 'Kitchens', 'Menues', function($scope, $stateParams, Kitchens, Menues) {
    var kId = $stateParams.kitchenId;
    $scope.$on('$ionicView.enter', function(e) {
      $scope.kitchen = Kitchens.get({id: kId});
      if(!$scope.menues) {
        $scope.menues = Menues.query({kitchenId: kId});
      }
    });
  }])
  .controller('DashCtrl', ['$scope', function($scope) {
    console.log('Welcome!');
  }])
  .controller('AccountCtrl', ['$scope', 'Auth', '$ionicHistory', '$state', function($scope, Auth, $ionicHistory, $state) {

    function validUser() {
      // @TODO: isUserTaken BE service call.
      return true;
    }

    function checkAuth() {
      // @TODO: isUserTokenAlive BE service call.

      // Get rid of this when BE login is finished
      return $scope.$parent.forcedLogin || window.localStorage.getItem('userData');
    }

    $scope.user = {};
    $scope.kitchen = null;

    $scope.register = function register() {
      if (!validUser()) {
        return false;
      }
      Auth.register($scope.user, function (res) {
        if (res.id) {
          // Successfull register
          $scope.user = res;
          // proceed then to login the user
          $scope.$parent.forcedLogin = true; // ugly man
          $scope.user = window.localStorage.setItem('userData', JSON.stringify(res));
          $ionicHistory.goBack();
        }
      });
    }

    $scope.logOut = function logOut() {
      window.localStorage.removeItem('userData');
      $state.go($state.current, {}, {reload: true});
    }

    $scope.nextStepButton = function nextStepButton() {
      //Show kitchen modal.
      //change button for register.
    }

    $scope.beAdmin = function beAdmin() {
      $scope.user.admin = true;
    }

    $scope.$on("$ionicView.beforeEnter", function(event, data){
      $scope.$parent.forcedLogin = false; // ugly man
      $scope.isAuth = checkAuth($scope.user);
      if (!$scope.isAuth) {
        $scope.user = {};
      } else {
        $scope.user = JSON.parse(window.localStorage.getItem('userData'));
      }
    });

  }]);
