angular.module('km.services', [])
  .factory('Kitchens', ['$resource', 'API', function ($resource, API) {
    return $resource(API+'/kitchens/:id');
  }])
  .factory('Menues', ['$resource', 'API', function ($resource, API) {
    return $resource(API+'/kitchens/:kitchenId/menues/:menuesId');
  }])
  .factory('Auth', ['$resource', 'API', function ($resource, API) {
    return {
      login: function (obj) {
        var registerAPI = API + (obj.admin ? '/users/admin' : '/users/consumer');
        return $resource(registerAPI, {
          password: obj.password,
          email: obj.email
        })
      },
      register: function (obj, callback) {
        if (obj.admin) {
          delete obj.admin;
          return $resource(API + '/users/admin').save(obj, callback);
        } else {
          return $resource(API + '/users/consumer').save(obj, callback);
        }
      }
    }

  }]);
